package com.freeking.replaymid2.utils;

import com.freeking.replaymid2.model.ListViewItem2;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-12.
 */
public class GetJapanListArr {

    ArrayList<ListViewItem2> listArr;


    public ArrayList<ListViewItem2> getListArr(String title) {
        ArrayList<ListViewItem2> listArr = new ArrayList<ListViewItem2>();

        switch (title) {
            case "쓰르라미 울 적에" :
                listArr.add(new ListViewItem2("쓰르라미 울 적에 - 01화", "http://183.110.25.100/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/10/20160625070907236gynjefhnm86je.flv?key1=46373839463932353035343531394431313930373331363933383530&key2=76B7C3C0CD46BA85798ABDF6C7CF06&ft=FC&class=normal&country=KR&pcode2=30168"));
                listArr.add(new ListViewItem2("쓰르라미 울 적에 - 02화", "http://183.110.25.100/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/11/201606250710311409c86qotvlxsid.flv?key1=39423239434634313735354431393231314130374131364232453239&key2=081D915DED3619207FA4566CE21D78&ft=FC&class=normal&country=KR&pcode2=72647"));
                listArr.add(new ListViewItem2("쓰르라미 울 적에 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vf1a2azyNyat6S5OOdt5fjT"));
                break;
            case "데스노트" :
                listArr.add(new ListViewItem2("데스노트 - 01화", "http://www.dailymotion.com/cdn/H264-848x480/video/x2wz2w1.mp4?auth=1468917351-2562-tulqqlh2-472b85853ebc929c82761e76c77c434a"));
                listArr.add(new ListViewItem2("데스노트 - 02화", "http://www.dailymotion.com/cdn/H264-848x480/video/x2yueru.mp4?auth=1468917373-2562-sayyw6cv-af52396be2ca8f14cd197fcdd241e899"));
                listArr.add(new ListViewItem2("데스노트 - 03화", "http://www.dailymotion.com/cdn/H264-848x480/video/x2ze0xc.mp4?auth=1468917395-2562-on3xh1id-3cee35e749aabb842f3923454c6979a1"));
                listArr.add(new ListViewItem2("데스노트 - 04화", "http://www.dailymotion.com/cdn/H264-848x480/video/x305yky.mp4?auth=1468917474-2562-nc0hjnlf-046ac66e08c0c0f2a789a7d38b60b130"));
                listArr.add(new ListViewItem2("데스노트 - 05화", "http://www.dailymotion.com/cdn/H264-848x480/video/x31h3jm.mp4?auth=1468917492-2562-kfymwsij-90312a635135896a022318a9fa985617"));
                listArr.add(new ListViewItem2("데스노트 - 06화", "http://www.dailymotion.com/cdn/H264-848x480/video/x32beht.mp4?auth=1468917517-2562-4yn559z1-9dd65020494bbb7156405e5886dbc64b"));
                listArr.add(new ListViewItem2("데스노트 - 07화", "http://www.dailymotion.com/cdn/H264-848x480/video/x3379qr.mp4?auth=1468917533-2562-cvqhlpxf-03253088d19ad2f5d5082b334a8aa1df"));
                listArr.add(new ListViewItem2("데스노트 - 08화", "http://www.dailymotion.com/cdn/H264-848x480/video/x34d5fq.mp4?auth=1468917549-2562-rmdehnb6-2b99a35b14948e7627da09c36abbe67d"));
                listArr.add(new ListViewItem2("데스노트 - 09화", "http://www.dailymotion.com/cdn/H264-848x480/video/x366xbt.mp4?auth=1468917568-2562-7phhy1ut-6b2e9992205df53fc03506c7a2f86e2d"));
                listArr.add(new ListViewItem2("데스노트 - 10화", "http://www.dailymotion.com/cdn/H264-848x480/video/x36u2oy.mp4?auth=1468917584-2562-enf4paqk-ee3915e3f288de1d027bc3fce46ccb69"));
                listArr.add(new ListViewItem2("데스노트 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v54bbYkaEEk1YF7ijEj79Wk"));
                break;
            case "시간을 달리는 소녀" :
                listArr.add(new ListViewItem2("시간을 달리는 소녀 - 01화", "http://183.110.26.42/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/89/20160714224834029ad4o9353guiia.flv?key1=44333036443233324334333932323831394330374431363038323343&key2=0603943CEE3270447BA93574E97477&ft=FC&class=normal&country=KR&pcode2=47844"));
                listArr.add(new ListViewItem2("시간을 달리는 소녀 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=ve462ToUCjI77I3zCKUTKCr"));
                listArr.add(new ListViewItem2("시간을 달리는 소녀 - 03화", "http://tvpot.cdn.videofarm.daum.net/vod/v5e88uEuDEDEWIDW5jjLLWx/mp4_360P_1M_T1/movie.mp4?px-time=1469976661&px-hash=cc9b62302cbabfe36dda07a64edce387&px-bps=1443214&px-bufahead=18"));
                listArr.add(new ListViewItem2("시간을 달리는 소녀 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v74f6GsNsa8aADbLGAiN9LG"));
                listArr.add(new ListViewItem2("시간을 달리는 소녀 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=va207wTwnTdTeEgjaTegBBE"));
                break;
            case "호프~기대제로의 신입사원" :
                listArr.add(new ListViewItem2("호프~기대제로의 신입사원 - 01-1화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v6536vGViviiGTW8Lv6Tp8p"));
                listArr.add(new ListViewItem2("호프~기대제로의 신입사원 - 01-2화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v9bd5HHHapVxHVnNtHRnVRG"));
                listArr.add(new ListViewItem2("호프~기대제로의 신입사원 - 02화", "http://183.110.26.79/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/14/2016072601350420339vvtvfou8pa1.flv"));
                listArr.add(new ListViewItem2("호프~기대제로의 신입사원 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v6999pbfqfbfHbzqOOGpzWJ"));
                break;
            case "장난스런 키스 Love in Tokyo 시즌1" :
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=vd4b7hGPphp4GEpZhpwY5aY&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=vd403TSaTaGo4xGq2GSSxSE&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v7e317CX77vPvR2Gl2AAGGp&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=vf3f6zzzxfxx3e8cZ8ttzEZ&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=va437ukBocQ3cRorGo6rmrg&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=vc6a3VDa5qusb7JSsJ5SF5u&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=vfef6GEibGG3GUEbMEfRUHU&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v120eH5wSTB55FShlSQNQkN&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=vdd2erXPrrNxS4N1nNRGx1D&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v6017XSX7X8d5SDKoDe5dee&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v42c6hZdmcLhFLTYvTj6LjZ&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=ve2a1SPS0PdPKVnm3nHeLL7&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v343eVEEbVsEOgbc3bIqXIw&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 14화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v8b2cpR3J6J6JEmJ1m9AEnz&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 15화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=ve749SEzjzBEGDN7vN2BS2S&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌1 - 16화 완결", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v7e93HB3Z3ZqIPiv3iI3PNI&play_loc=undefined&alert=true"));
                break;
            case "장난스런 키스 Love in Tokyo 시즌2" :
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=wB4y9CLv31g$"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=6FchbkIUXlQ$"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=JkLhXW8HfGM$"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=qFc8XWANrGk$"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=SVo0LoVF92c$"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=1vyamzmMyPg$"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=CIkk1SfMf6Q$"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=k3tTkYq5e08$"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=4SiMBo0gxI0$"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=7lAhsn8x1vQ$"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=i9HBIY_Vgw4$"));
                //listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 12화", ""));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v9e6btnjRsCsZnZHjPjDHPE"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 14화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v4397GHGhiVGvUhDGDvGooV"));
                listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 15화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vf2af0ktQgkl4xkqX900gXg"));
                //listArr.add(new ListViewItem2("장난스런 키스 Love in Tokyo 시즌2 - 16화 완결", ""));
                break;
            case "옆자리 세키군" :
                listArr.add(new ListViewItem2("옆자리 세키군 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v37d6lmBAiAl5V6BmmcVnm6"));
                listArr.add(new ListViewItem2("옆자리 세키군 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=va9begAEQbyhbcGAjyVygCA"));
                listArr.add(new ListViewItem2("옆자리 세키군 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v22e63uwMXvwMvM3hIuhKYk"));
                listArr.add(new ListViewItem2("옆자리 세키군 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v1534TXTsTwTynIM8IfMbXX"));
                listArr.add(new ListViewItem2("옆자리 세키군 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v7f6aNJdNVWPVaSONzNSSzc"));
                listArr.add(new ListViewItem2("옆자리 세키군 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vef9fz6iJiIbMReEfMEiedJ"));
                listArr.add(new ListViewItem2("옆자리 세키군 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v06c7LkQZQZLLufH8nffjnu"));
                listArr.add(new ListViewItem2("옆자리 세키군 - 08화 완결", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v127cBNbhfjjhCs4F4ZsC0F"));
                break;
            case "그리고 아무도 없었다" :
                listArr.add(new ListViewItem2("그리고 아무도 없었다 - 01-1화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v8e20FpORpvOkxqgvFFUqLp"));
                listArr.add(new ListViewItem2("그리고 아무도 없었다 - 01-2화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v609exluWWfCW7vzFuulC1u"));
                break;
            case "쿠로사키군의 말대로는 되지 않아" :
                listArr.add(new ListViewItem2("쿠로사키군의 말대로는 되지 않아 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vec2atuG1WPPGpMGPv6MtuP"));
                listArr.add(new ListViewItem2("쿠로사키군의 말대로는 되지 않아 - 02화 완결", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v4541Qk7HJO7RZHPQn00k07"));
                break;
            case "도쿠야마 다이고로를 누가 죽였나" :
                listArr.add(new ListViewItem2("도쿠야마 다이고로를 누가 죽였나 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vbcf0KzqBqSzS7DgvmJqgJS"));
                listArr.add(new ListViewItem2("도쿠야마 다이고로를 누가 죽였나 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc1322soV662syVTAwTw999"));
                listArr.add(new ListViewItem2("도쿠야마 다이고로를 누가 죽였나 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v47bdUcvfp7LHt0c0MNfL2N"));
                listArr.add(new ListViewItem2("도쿠야마 다이고로를 누가 죽였나 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v8fd09A9FbJZ92FuZJta0t2&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("도쿠야마 다이고로를 누가 죽였나 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=ved93TgPbRkTZAZDZ2A6Z66"));
                listArr.add(new ListViewItem2("도쿠야마 다이고로를 누가 죽였나 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=ve197WH2dWgHW3bMbT3Son2"));
                listArr.add(new ListViewItem2("도쿠야마 다이고로를 누가 죽였나 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v995fXdtlo001xl1l5xouY9"));
                listArr.add(new ListViewItem2("도쿠야마 다이고로를 누가 죽였나 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vb244UW8NwYWSHlKNoRWHRl"));
                listArr.add(new ListViewItem2("도쿠야마 다이고로를 누가 죽였나 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vfc98U3CDakCgWzhJhzaXo4"));
                listArr.add(new ListViewItem2("도쿠야마 다이고로를 누가 죽였나 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc5a6gb25bg95zGhgogdgh9"));
                break;
            case "사폐" :
                listArr.add(new ListViewItem2("사폐 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vafd3DqEuEFZEUFO3ZJPq3q"));
                listArr.add(new ListViewItem2("사폐 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v20b3zo2CoRb2c0VCl2bVVC"));
                listArr.add(new ListViewItem2("사폐 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=va4c2sBcRuquQDx7sxDDgEx"));
                listArr.add(new ListViewItem2("사폐 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vb063mRkTk1ww8ngk8gFAcm"));
                listArr.add(new ListViewItem2("사폐 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v12ddexxV49zx9kV4VrxRox"));
                listArr.add(new ListViewItem2("사폐 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v36ca6W6M3jUejWMauWDMM6"));
                listArr.add(new ListViewItem2("사폐 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v6e04iiHhddHytsXDiQYDX2"));
                listArr.add(new ListViewItem2("사폐 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v5245yNyNxGN3fq338D3y3D"));
                break;
            case "사쿠라신쥬" :
                listArr.add(new ListViewItem2("사쿠라신쥬 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/94/20110325183405998lh8fgeaj9ftjw.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/68/20110325191326500fc7t7b9d20nk2.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/15/20110325193259687ldjnhyw3o7q2v.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/06/2011032520125619516ur6lsevqia5.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/43/2011032520324665000oz58jrupmu2.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/32/20110325205157349emmqvwerxo3mm.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/62/201103252111243790vlg3x475q0b6.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/83/20110325213058785u10ppqkd9cajm.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/16/201103252211456453wnxkdfl1i7w9.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/80/20110325224617910jkxyobbu863kr.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/54/20110325232849741jmhx50ulawp15.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/16/201103260002359197ov9usynrce5e.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 13화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/86/2011032621211911656arjoy2ut68e.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 14화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/85/20110326010832915cahr7bfy0hdo9.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 15화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/81/20110326013908029z6s4zo8g2s9oz.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 16화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/69/20110326020906043lemi09auxha96.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 17화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/85/20110326022334052n4lezl36dkkm6.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 18화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/13/20110326025443469gnxnd55u2gfjg.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 19화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/55/201103260325116607zti79uzwccyq.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 20화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/12/201103260355399290x6ogle8hyfax.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 21화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/71/20110326042643626gbqeyoz5p20sd.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 22화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/64/20110326045709426jmoj7w8mlm6rh.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 23화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/73/20110326052746384bzvany8awjlea.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 24화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/94/20110326055950576z24lr8ujm5n8k.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 25화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/62/20110326215529663xlarw6p9tqo9h.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 26화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/08/20110327090255835gfljf93n1r6jy.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 27화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/57/20110327093749273r5vtjkgu06uu6.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 28화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/93/20110327101110257karh0u2le4mq9.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 29화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/04/2011032710540833525drgomtt9zo7.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 30화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/08/20110330141119023pgjuvjtfnifid.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 31화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/05/2011032712032602387dhvopfzq6dl.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 32화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/89/20110327123552726l7yg8lu12n210.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 33화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/83/20110330144912569qornxr1r07wdl.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 34화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/05/201103301652366481mao09cucrbgh.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 35화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/28/20110330173002757fnji0ghhulrdb.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 36화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/57/20110330202715085eikrpawewc6hi.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 37화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/19/20110330184200304072gxkiq7abcn.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 38화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/82/20110330185903179mkw423qzh4p4q.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 39화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/08/201103302101521167l33idm58d1qq.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 40화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/99/20110330213518944nomfdwv7dli3g.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 41화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/43/2011041508125229343g93r1pbsoc8.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 42화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/68/20110415085222918w7wc3a619ew87.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 43화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/24/20110415092854699ajqtb23u13c76.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 44화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/60/201104151029084182mh9xyy7xvkb3.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 45화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/99/2011041511033162132bxxjuv8wiu6.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 46화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/29/20110415113652199j722lxxqjjupf.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 47화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/90/201104151210150598i64v05dxr3re.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 48화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/52/20110415124328105qpe4qt3oovgek.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 49화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/30/20110415131630637xgnq62swgmofi.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 50화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/34/201104151349358092icw6h7gl6v1q.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 51화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/33/20110415142314277006yet5h45f5j.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 52화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/57/20110415145634449h6amx5jh0y51n.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 53화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/69/20110415152934604y88ibhj3ki0pd.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 54화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/85/201104151745493290j5kqvups9qrp.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 55화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/82/20110415180147854m5yms4tjyd67n.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 56화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/71/20110415182458923218890g8pl614.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 57화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/85/20110415184121514obmuplgcoku15.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 58화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/50/20110415191313798f7jmvhvjk6g6s.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 59화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/93/20110415194456423z2940r0cqykis.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 60화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/55/201104152015501502tkmiutet2g1s.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 61화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/67/20110415204709209bj4lhsukn0i9w.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 62화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/w/l/wl3520/54/201104152102032856of8eo6l0esx6.flv"));
                listArr.add(new ListViewItem2("사쿠라신쥬 - 63화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/w/l/wl3520/28/20110415213539196e4ydbjdhueql7.flv"));
                break;
            case "세상에서 가장 어려운 사랑" :
                listArr.add(new ListViewItem2("세상에서 가장 어려운 사랑 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/68/20160414220843023pg2h3jk39henp.flv"));
                listArr.add(new ListViewItem2("세상에서 가장 어려운 사랑 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/41/20160421181509370ach364i8vd5ft.flv"));
                listArr.add(new ListViewItem2("세상에서 가장 어려운 사랑 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/98/2016042813103066781l7xq3khz0ni.flv?key1=43354342343731303230323830343632384330384331364135313230&key2=F696900096CD93D385D20031989481&ft=FC&class=normal&country=KR&pcode2=89150"));
                listArr.add(new ListViewItem2("세상에서 가장 어려운 사랑 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/49/20160505152750839cbbaf0x7011xk.flv?key1=42454534393132373230323930344532383030383231363337373830&key2=DCA67E2E7698A0E0DC405D1D77ABAA&ft=FC&class=normal&country=KR&pcode2=5938"));
                listArr.add(new ListViewItem2("세상에서 가장 어려운 사랑 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/70/20160512185753911pvnqc38j28luf.flv?key1=43464241443534363730323030344232384330384431363341334331&key2=6AE381AC815793D519C2D8E384906C&ft=FC&class=normal&country=KR&pcode2=81407"));
                listArr.add(new ListViewItem2("세상에서 가장 어려운 사랑 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/15/20160519135614503hu3imyfy7ml5t.flv?key1=35383342333430373630333530343132383830384631363546453135&key2=64BA0F1E085CCEFE6C3919540FBA18&ft=FC&class=normal&country=KR&pcode2=21731"));
                listArr.add(new ListViewItem2("세상에서 가장 어려운 사랑 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/49/20160527025304356c82jm6k3w5pas.flv?key1=33364538333932353530334230343032383030384631364633463841&key2=12ED2B55275AEAA1126153175FE06A&ft=FC&class=normal&country=KR&pcode2=56352"));
                listArr.add(new ListViewItem2("세상에서 가장 어려운 사랑 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/55/20160603004943217nv7x3w4isn2u8.flv?key1=31333930354534324630334630343932384430384531363135323434&key2=A8CE7BC17C92BDFBDC4ECE8A78B8A3&ft=FC&class=normal&country=KR&pcode2=88703"));
                listArr.add(new ListViewItem2("세상에서 가장 어려운 사랑 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vdc77RgRnH5g9At0zek9RPp"));
                listArr.add(new ListViewItem2("세상에서 가장 어려운 사랑 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/02/201606161443522918pzp4sxygev52.flv"));
                break;
            case "호타루의 빛 시즌1" :
                listArr.add(new ListViewItem2("호타루의 빛 시즌1 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/70/20121218180330723zvelu1f9ppc9q.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌1 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/75/20121218180352645f9iklo88lp06g.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌1 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/76/20121218180415051panusa5085clk.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌1 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/79/201212181804498323z11mty5o96ez.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌1 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/85/20121218180516645o5v0v75nidjik.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌1 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/y/b/ybhldm81/32/20100718152108114azprjzvs7qt1l.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌1 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/93/2012121818061869190yztpo38dj11.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌1 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/97/20121218180645941wb8td9ef6lh5d.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌1 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/98/20121218180712848quivdxt3o5rn6.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌1 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/99/20121218180750566ui0zjjleeu3ym.flv"));
                break;
            case "호타루의 빛 시즌2" :
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/t/n/tnwjdkr/82/20100726185252873umjj8g4m8bdgc.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/t/n/tnwjdkr/57/20100727225226995xa6l9yy7rx362.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/t/n/tnwjdkr/89/20100726112709779msw0une4ap2z2.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/t/n/tnwjdkr/86/20100730113242404q4wifbi2ju6ui.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/t/n/tnwjdkr/71/20100806204531063b4js5rwvn6zg1.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/t/n/tnwjdkr/80/201008140322067194h4e57nne0ptt.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/38/20121221005222488iptnf8v4osk6s.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/t/n/tnwjdkr/19/20100830234137206ioitx8xiwf924.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/t/n/tnwjdkr/21/201009031251418044291jwh19bvpf.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/t/n/tnwjdkr/45/201009121447202600l30fnxtp2r7w.flv"));
                listArr.add(new ListViewItem2("호타루의 빛 시즌2 - 11화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/t/n/tnwjdkr/46/20130718124649906f6i41t1wrhx9n.flv"));
                break;
            case "프로포즈 대작전" :
                listArr.add(new ListViewItem2("프로포즈 대작전 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/d/j/djkim778/20110105153637358ps6vihp92uz0n.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/d/j/djkim778/20110110081305285v5nxnix2wexdl.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/34/201505280302488173eyi6xkajqkka.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/d/j/djkim778/20110110081201129gpy0u23qotgj8.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/92/20150528033416645qjfjwnh3mtabf.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/d/j/djkim778/20110110075528676vsmi4em4o79d9.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/d/j/djkim778/20110110075535160abifb0zvqvv8d.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/d/j/djkim778/20110110075545004zv6uodd0qgtri.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/d/j/djkim778/2011011007555466069oiad8l53p5t.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/d/j/djkim778/20110110075555410ht7g85fzc528p.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/12/20150528051746114bvxkuputheihb.flv"));
                listArr.add(new ListViewItem2("프로포즈 대작전 - sp", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/w/sweetyfairy/23/20120622095754672w8o43n2zvs92u.flv"));
                break;
            case "전개걸" :
                listArr.add(new ListViewItem2("전개걸 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/94/20150223070738515idydjtxq1slay.flv"));
                listArr.add(new ListViewItem2("전개걸 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/s/i/simaart99/11/20150223072511687m7dsf0xlbw38p.flv"));
                listArr.add(new ListViewItem2("전개걸 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/35/20150223075236046z7yghaomb2tg1.flv"));
                listArr.add(new ListViewItem2("전개걸 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/43/201502230810561718mlr8qxu8vb7z.flv"));
                listArr.add(new ListViewItem2("전개걸 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/00/20150223084751937cjzz94bqc900d.flv"));
                listArr.add(new ListViewItem2("전개걸 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/12/20150223090312718pmj85ne3baii3.flv"));
                listArr.add(new ListViewItem2("전개걸 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/90/20150223121711906y3wc956q2svk0.flv"));
                listArr.add(new ListViewItem2("전개걸 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/09/201502231311424533hzhfaecxipl9.flv"));
                listArr.add(new ListViewItem2("전개걸 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/34/20150223192751453a8x18zc78mh7o.flv"));
                listArr.add(new ListViewItem2("전개걸 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/93/20150223194353328bp5oq4bx63d8v.flv"));
                listArr.add(new ListViewItem2("전개걸 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/79/20150223195922875qy5nw65w57mqm.flv"));
                break;
            case "아네고" :
                listArr.add(new ListViewItem2("아네고 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/89/201502141223517182vp1x760iwja2.flv"));
                listArr.add(new ListViewItem2("아네고 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/94/20150214122501328yrtgg30u9349f.flv"));
                listArr.add(new ListViewItem2("아네고 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/99/20150214122611984ydiyxg1olgvzf.flv"));
                listArr.add(new ListViewItem2("아네고 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/03/20150214122719781f7l8r50pqmlye.flv"));
                listArr.add(new ListViewItem2("아네고 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/07/20150214122829140xxc6tzct3qc7s.flv"));
                listArr.add(new ListViewItem2("아네고 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/08/20150214122933343edc18txfa3nul.flv"));
                listArr.add(new ListViewItem2("아네고 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/10/20150214123036593iurf7xdj4fjky.flv"));
                listArr.add(new ListViewItem2("아네고 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/16/20150214123152468ci1m7yssie4kk.flv"));
                listArr.add(new ListViewItem2("아네고 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/22/20150214123310890apu8hw9qldsna.flv"));
                listArr.add(new ListViewItem2("아네고 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/25/20150214123430515bq5f2jvn1cgk1.flv"));
                listArr.add(new ListViewItem2("아네고 - 11화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/j/n/jnonda00/92/20100731220227493q3ga7rjaxkrg8.flv"));
                listArr.add(new ListViewItem2("아네고 - 스페셜-1", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=yVdOan0uveQ$"));
                listArr.add(new ListViewItem2("아네고 - 스페셜-2", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=2nFPq5vu9MU$"));
                break;
            case "사프리" :
                listArr.add(new ListViewItem2("사프리 - 01화", "http://cmvs.mgoon.com/storage2/m2_video/2008/09/17/1693853.flv?px-bps=1377706&px-bufahead=10"));
                listArr.add(new ListViewItem2("사프리 - 02화", "http://cmvs.mgoon.com/storage3/m2_video/2008/09/17/1694111.flv?px-bps=1320381&px-bufahead=10"));
                listArr.add(new ListViewItem2("사프리 - 03화", "http://cmvs.mgoon.com/storage4/m2_video/2009/10/07/2611395.mp4?px-bps=1422498&px-bufahead=10"));
                listArr.add(new ListViewItem2("사프리 - 04화", "http://cmvs.mgoon.com/storage4/m2_video/2008/09/18/1694839.flv?px-bps=1337196&px-bufahead=10"));
                listArr.add(new ListViewItem2("사프리 - 05화", "http://cmvs.mgoon.com/storage3/m2_video/2008/09/18/1695035.flv?px-bps=1338480&px-bufahead=10"));
                listArr.add(new ListViewItem2("사프리 - 06화", "http://cmvs.mgoon.com/storage4/m2_video/2008/09/18/1695020.flv?px-bps=1345500&px-bufahead=10"));
                listArr.add(new ListViewItem2("사프리 - 07화", "http://cmvs.mgoon.com/storage3/m2_video/2008/09/18/1695381.flv?px-bps=1304058&px-bufahead=10"));
                listArr.add(new ListViewItem2("사프리 - 08화", "http://cmvs.mgoon.com/storage2/m2_video/2008/09/18/1695458.flv?px-bps=1351905&px-bufahead=10"));
                listArr.add(new ListViewItem2("사프리 - 09화", "http://cmvs.mgoon.com/storage3/m2_video/2008/09/18/1695513.flv?px-bps=1407436&px-bufahead=10"));
                listArr.add(new ListViewItem2("사프리 - 10화", "http://cmvs.mgoon.com/storage2/m2_video/2008/09/18/1695629.flv?px-bps=1407582&px-bufahead=10"));
                listArr.add(new ListViewItem2("사프리 - 11화", "http://cmvs.mgoon.com/storage1/m2_video/2008/09/18/1695714.flv?px-bps=1407807&px-bufahead=10"));
                break;
            case "스타맨 : 이 별의 사랑" :
                listArr.add(new ListViewItem2("스타맨 : 이 별의 사랑 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=03SuQIZ7Umk$"));
                listArr.add(new ListViewItem2("스타맨 : 이 별의 사랑 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Csx2Uo2NvIc$"));
                listArr.add(new ListViewItem2("스타맨 : 이 별의 사랑 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=xH5wXxEptNg$"));
                listArr.add(new ListViewItem2("스타맨 : 이 별의 사랑 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=oqYpiwJ9TdU$"));
                listArr.add(new ListViewItem2("스타맨 : 이 별의 사랑 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=AG_RSP3ymlc$"));
                listArr.add(new ListViewItem2("스타맨 : 이 별의 사랑 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vpdz1zCvo_o$"));
                listArr.add(new ListViewItem2("스타맨 : 이 별의 사랑 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=acsrNZzYb8c$"));
                listArr.add(new ListViewItem2("스타맨 : 이 별의 사랑 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Wtxns5urwNU$"));
                listArr.add(new ListViewItem2("스타맨 : 이 별의 사랑 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=KSoZFbNSntk$"));
                listArr.add(new ListViewItem2("스타맨 : 이 별의 사랑 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=aCzs0aPoqnk$"));
                break;
            case "오늘은 회사 쉬겠습니다" :
                listArr.add(new ListViewItem2("오늘은 회사 쉬겠습니다 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/76/20141018064730781vcpmc23o9mvzt.flv"));
                listArr.add(new ListViewItem2("오늘은 회사 쉬겠습니다 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/k/c/kcrlove88/10/20150623174211110bmpooqsx95lo8.flv"));
                listArr.add(new ListViewItem2("오늘은 회사 쉬겠습니다 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/33/201410301815541096mtpzhxyuy5b8.flv"));
                listArr.add(new ListViewItem2("오늘은 회사 쉬겠습니다 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/38/20150408234754531d6osfrft1491m.flv"));
                listArr.add(new ListViewItem2("오늘은 회사 쉬겠습니다 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/34/20141115124809955xpkzlh85d2asy.flv"));
                listArr.add(new ListViewItem2("오늘은 회사 쉬겠습니다 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/40/20141124171708383vsssyd9udysue.flv"));
                listArr.add(new ListViewItem2("오늘은 회사 쉬겠습니다 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/s/i/simaart99/38/20150409004059828ydz7suq4dokyp.flv"));
                listArr.add(new ListViewItem2("오늘은 회사 쉬겠습니다 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/33/20141207232730406xf41twqcvy7gt.flv"));
                listArr.add(new ListViewItem2("오늘은 회사 쉬겠습니다 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/43/201412170206598127ligjcgzd457x.flv"));
                listArr.add(new ListViewItem2("오늘은 회사 쉬겠습니다 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/41/20141219165613567un4xzw9cig6w8.flv"));
                break;
            case "라스트 신데렐라" :
                listArr.add(new ListViewItem2("라스트 신데렐라 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=vffacIMTTG8ji6syAIsGIE6&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("라스트 신데렐라 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=vbe1foZZTTloVvUSVTUTT9g&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("라스트 신데렐라 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v8fc1p6I8O186Qa2oOas1R1&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("라스트 신데렐라 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v0532Qbxpbpxe95eUQ5xbeb&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("라스트 신데렐라 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=vcb09vMQMJamMMFEivFdd7M&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("라스트 신데렐라 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v0e60VY0Z00SVTeAOzYTZ6Y&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("라스트 신데렐라 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=va46epq3k72qrSQCE3dXCCX&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("라스트 신데렐라 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v4fd28qQOsQWO94NEmTJWq9&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("라스트 신데렐라 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v0b6fNNNpNA3NaAbx36AbpI&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("라스트 신데렐라 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=va393FZdKPKZt4rkjZgrahC&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("라스트 신데렐라 - 11화 완결", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v79b5wqvNq0s9i960q44ZZZ&play_loc=undefined&alert=true"));
                break;
            case "마더게임 : 그녀들의 계급" :
                listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc055Ol72qOWvjGqjOq17us"));
                //listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 02화", ""));
                listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v9402l3nkn2Zl0CtUzlzfUZ"));
                listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v7fcaLxLGKJKhlUzzOlUBJB"));
                listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vbfe1ALfPf8AxxpZbXjPAMm"));
                listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v95c8ppduOcdoloCOnwllAu"));
                //listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 07화", ""));
                //listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 08화", ""));
                //listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 09화", ""));
                //listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 10화", ""));
                //listArr.add(new ListViewItem2("마더게임 : 그녀들의 계급 - 11화", ""));
                break;
            case "노다메 칸타빌레" :
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 01화", "http://cmvs.mgoon.com/storage2/m2_video/2008/03/29/1461977.flv?px-bps=1275418&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 02화", "http://cmvs.mgoon.com/storage1/m2_video/2008/03/29/1461993.flv?px-bps=1425043&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 03화", "http://cmvs.mgoon.com/storage1/m2_video/2008/03/29/1462013.flv?px-bps=1366275&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 04화", "http://cmvs.mgoon.com/storage2/m2_video/2008/03/29/1462063.flv?px-bps=1387785&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 05화", "http://cmvs.mgoon.com/storage4/m2_video/2008/03/29/1462073.flv?px-bps=1349187&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 06화", "http://cmvs.mgoon.com/storage3/m2_video/2008/03/29/1462081.flv?px-bps=1280677&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 07화", "http://cmvs.mgoon.com/storage2/m2_video/2008/03/29/1462089.flv?px-bps=1371892&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 08화", "http://cmvs.mgoon.com/storage1/m2_video/2008/03/29/1462100.flv?px-bps=1365015&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 09화", "http://cmvs.mgoon.com/storage3/m2_video/2008/03/29/1462108.flv?px-bps=1353654&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 10화", "http://cmvs.mgoon.com/storage4/m2_video/2008/03/29/1462123.flv?px-bps=1318995&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 11화", "http://cmvs.mgoon.com/storage3/m2_video/2008/03/29/1462150.flv?px-bps=1425765&px-bufahead=10"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 최종악장 전편", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/m/o/mongpa36/44/20111119041945896pjxx18bkippyr.flv"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 최종악장 후편", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/a/o/aoiyoo/72/20140928225811547d4kkerxfb7qp1.flv"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 인 유럽", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=FycALBSTghg$"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 인 유럽2008 - 1", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/r/e/reginaksm/54/201110222356404110lxnovj5g81y5.flv"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 인 유럽2008 - 2", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/r/e/reginaksm/81/2011102300280145832isqzzjknggl.flv"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 인 유럽2008 - 3", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/r/e/reginaksm/64/20111023052241865su470tfkoms0d.flv"));
                listArr.add(new ListViewItem2("노다메 칸타빌레 - 인 유럽2008 - 4", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/r/e/reginaksm/08/20111023205958099vwrzggaj35z0m.flv"));
                break;
            case "문제 있는 레스토랑" :
                listArr.add(new ListViewItem2("문제 있는 레스토랑 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/29/20150116005245406lknnrjtnbw64w.flv"));
                listArr.add(new ListViewItem2("문제 있는 레스토랑 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/06/20150205180853406353reuk0mats7.flv"));
                listArr.add(new ListViewItem2("문제 있는 레스토랑 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/16/201502051809538753fudemsz3br2i.flv"));
                listArr.add(new ListViewItem2("문제 있는 레스토랑 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/52/20150216173322281g7tzalat0ccp6.flv"));
                listArr.add(new ListViewItem2("문제 있는 레스토랑 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/65/20150217145817453ap88ttz1fqzr1.flv"));
                listArr.add(new ListViewItem2("문제 있는 레스토랑 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vce5bb3bM64b1Gk3qkq68Oq"));
                listArr.add(new ListViewItem2("문제 있는 레스토랑 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v2584jD5k5bmDaq1JEkm0nT"));
                listArr.add(new ListViewItem2("문제 있는 레스토랑 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v607cWnnAn00UVASgVvWn2g"));
                listArr.add(new ListViewItem2("문제 있는 레스토랑 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v22cbsg0NghHs8bgIhvgSIO"));
                listArr.add(new ListViewItem2("문제 있는 레스토랑 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vf66brmrrPoPS7mOTX2TPS7"));
                break;
            case "고독한 미식가 시즌3" :
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=qEg0cT1Zqic$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=-8et7bgvS78$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=_mLDvz7uYUw$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=8vORgT_6Jpk$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=qytb51cnlH0$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=WiEi5mpLnMA$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=0a98XPW6p84$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=GhQlxDZ6KpU$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=-mF1yMzTlFY$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=AKpB6cTIJxA$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=WElqoJ0VD00$"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌3 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=2INshFtUB5M$"));
                break;
            case "고독한 미식가 시즌4" :
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v79e4Qel41OceOG4rr1c1rD"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v8c5dJ3bYEqyWlAZ9YKpEfY"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v58b49UUDUgUI8fkI9fIyoU"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v54f2BRiRBOyvH7qyziOquU"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vb026lJdqdqlCPCppCAJJ99"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v4058PiPuNbLbUlZZNlWJqP"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - SP", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v3eaancinwnirr6CAAxiVA5"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vd662xuzuz7xDB86u6ZJ2Fu"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc33805lBpUpa4MTppaaeP4"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vf0d1Lv6r3Y3vkgrkLQjkjY"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v9d44JHCwC3l3w3AwEGHJGh"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v2febBZtIrZrFeZFFFe1Zct"));
                listArr.add(new ListViewItem2("고독한 미식가 시즌4 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v867ekxFkNTNWyuxkcTkQxT"));
                break;
            case "수족관 걸" :
                listArr.add(new ListViewItem2("수족관 걸 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/c/o/codeno001/54/20160712003009808a0yanwb2ybhdr.flv?key1=44423341353532393833363630324630344330393331364633303445&key2=42F6BAD1C80F84C23CF9A595B1FD36&ft=FC&class=normal&country=KR&pcode2=78425"));
                listArr.add(new ListViewItem2("수족관 걸 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/46/20160716122148587vxgryzgguis7t.flv"));
                listArr.add(new ListViewItem2("수족관 걸 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/c/o/codeno001/48/201607161221579720zs4qul5q4ruo.flv?key1=46354534314532314533373030323130343830394431363030464242&key2=389A3092467AE9A43D0591DC41ED3A&ft=FC&class=normal&country=KR&pcode2=85721"));
                listArr.add(new ListViewItem2("수족관 걸 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vb848YcAuCHYYurjOHArqMu"));
                listArr.add(new ListViewItem2("수족관 걸 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc924Rjl9DgHgg2VleVjDel"));
                //listArr.add(new ListViewItem2("수족관 걸 - 06화", ""));
                //listArr.add(new ListViewItem2("수족관 걸 - 07화", ""));
                break;
            case "ON 이상범죄수사관 토도 히나코" :
                listArr.add(new ListViewItem2("ON 이상범죄수사관 토도 히나코 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v0533GbkaNqNQqwZeQ3YY3d"));
                listArr.add(new ListViewItem2("ON 이상범죄수사관 토도 히나코 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=va190WuXdMcXwQE6RMaMcbi"));
                listArr.add(new ListViewItem2("ON 이상범죄수사관 토도 히나코 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=va6b3IOzfKEf9RZNaR9ESTN"));
                listArr.add(new ListViewItem2("ON 이상범죄수사관 토도 히나코 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=ve7d3RzyzZRzZw3kR3jRZDw"));
                listArr.add(new ListViewItem2("ON 이상범죄수사관 토도 히나코 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v735f0WWjXKKooaLXLxQQzc"));
                listArr.add(new ListViewItem2("ON 이상범죄수사관 토도 히나코 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vafcafUpCfQUrEeCo2GC1Qw"));
                listArr.add(new ListViewItem2("ON 이상범죄수사관 토도 히나코 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v3864hcVGVxLvZ9CcTA6b6k"));
                listArr.add(new ListViewItem2("ON 이상범죄수사관 토도 히나코 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/01/201609030829291543lrnhpxhfy41g.flv"));
                //listArr.add(new ListViewItem2("ON 이상범죄수사관 토도 히나코 - 09화", ""));
                break;
            case "하드너트!수학소녀가 좋아하는 사건부" :
                listArr.add(new ListViewItem2("하드너트!수학소녀가 좋아하는 사건부 - 01화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDJ6zJpqzj4LJhZELjSRYNxlyojjpTREtq1n9tnhRMYjgw7CQFPNodztp5kleN7R9tt2RaEaa4Gr&type=pseudo"));
                listArr.add(new ListViewItem2("하드너트!수학소녀가 좋아하는 사건부 - 02화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDJ6zJpqzj4LJhZELjSRYNxJwvL2Zl0v0ipEQs1jPPcAxgw7CQFPNodzjv0HoGqGymdt2RaEaa4Gr&type=pseudo"));
                listArr.add(new ListViewItem2("하드너트!수학소녀가 좋아하는 사건부 - 03화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDJ6zJpqzj4LJhZELjSRYNwKPJfJLmvQnDxP56k8CycBgw7CQFPNody7EZum268M7Nt2RaEaa4Gr&type=pseudo"));
                listArr.add(new ListViewItem2("하드너트!수학소녀가 좋아하는 사건부 - 04화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDJ6zJpqzj4LJhZELjSRYNwEjiijLmyJfxLa7O3UYisHPisgw7CQFPNodz8S3N9t0pq29t2RaEaa4Gr&ch=true&type=pseudo"));
                listArr.add(new ListViewItem2("하드너트!수학소녀가 좋아하는 사건부 - 05화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvCANe0Sp2EogJhZELjSRYNwEjiijLmyJfxCgKxKRWk76egw7CQFPNodyJdq5MILL7Att2RaEaa4Gr&type=pseudo"));
                listArr.add(new ListViewItem2("하드너트!수학소녀가 좋아하는 사건부 - 06화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDJ6zJpqzj4LJhZELjSRYNyW4IpD2Kwis39r397c8snB5gw7CQFPNodxOlip4Dyky92Nt2RaEaa4Gr&type=pseudo"));
                listArr.add(new ListViewItem2("하드너트!수학소녀가 좋아하는 사건부 - 07화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvCANe0Sp2EogJhZELjSRYNyZDBfip3sCRhUL82LdTatVKgw7CQFPNodwsFn1P0ZrpK9t2RaEaa4Gr&type=pseudo"));
                listArr.add(new ListViewItem2("하드너트!수학소녀가 좋아하는 사건부 - 08화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDJ6zJpqzj4LJhZELjSRYNyD9mQeD98CuBjBMhCyZyu3gw7CQFPNodxzZ4uBtgEWiidt2RaEaa4Gr&ch=true&type=pseudo"));
                break;
            case "목소리 사랑" :
                listArr.add(new ListViewItem2("목소리 사랑 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v18d7pfplpb7lklxc3Okktf"));
                listArr.add(new ListViewItem2("목소리 사랑 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v5d1dnhAOnhWNhHUEGvHGE2"));
                listArr.add(new ListViewItem2("목소리 사랑 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/43/20160729155545632l6c7k9olalurc.flv"));
                listArr.add(new ListViewItem2("목소리 사랑 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v0351qZt5oawE6HEowa6KQ5"));
                listArr.add(new ListViewItem2("목소리 사랑 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=ve9f1kQ4CIChmbOwmVIkQhh"));
                listArr.add(new ListViewItem2("목소리 사랑 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/c/o/codeno001/13/20160906062711701oq54jc7tpomf9.flv?key1=31443844303134353235323432334330364530393431363032384535&key2=12E100777A23E2DE683E053E01E11B&ft=FC&class=normal&country=KR&pcode2=45012"));
                listArr.add(new ListViewItem2("목소리 사랑 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/vld/_user/c/o/codeno001/14/201609060627454446ji5jokqobpsd.flv?key1=35414437383130364235333032333430364530393431364334463644&key2=425298729809221638DB774DEC5D45&ft=FC&class=normal&country=KR&pcode2=9388"));
                //listArr.add(new ListViewItem2("목소리 사랑 - 08화", ""));
                //listArr.add(new ListViewItem2("목소리 사랑 - 09화", ""));
                //listArr.add(new ListViewItem2("목소리 사랑 - 10화", ""));
                //listArr.add(new ListViewItem2("목소리 사랑 - 11화", ""));
                break;
            case "사채꾼 우시지마 시즌1" :
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌1 - 01화", "http://cmvs.mgoon.com/storage4/m2_video/2010/10/18/4182164.mp4?px-bps=1428390&px-bufahead=10"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌1 - 02화", "http://cmvs.mgoon.com/storage2/m2_video/2010/10/24/4193075.mp4?px-bps=1435269&px-bufahead=10"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌1 - 03화", "http://cmvs.mgoon.com/storage3/m2_video/2010/10/29/4202165.mp4?px-bps=1420086&px-bufahead=10"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌1 - 04화", "http://cmvs.mgoon.com/storage3/m2_video/2010/11/07/4216112.mp4?px-bps=1428141&px-bufahead=10"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌1 - 05화", "http://cmvs.mgoon.com/storage3/m2_video/2010/11/16/4231511.mp4?px-bps=1421019&px-bufahead=10"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌1 - 06화", "http://cmvs.mgoon.com/storage4/m2_video/2010/11/22/4241392.mp4?px-bps=1437498&px-bufahead=10"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌1 - 07화", "http://cmvs.mgoon.com/storage3/m2_video/2010/12/05/4259608.mp4?px-bps=1416615&px-bufahead=10"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌1 - 08화", "http://cmvs.mgoon.com/storage1/m2_video/2010/12/14/4273255.mp4?px-bps=1413843&px-bufahead=10"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌1 - 09화 완결", "http://cmvs.mgoon.com/storage1/m2_video/2010/12/20/4282206.mp4?px-bps=1412479&px-bufahead=10"));
                break;
            case "사채꾼 우시지마 시즌2" :
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌2 - 01화", "http://cmvs.mgoon.com/storage3/m2_video/2014/01/26/5613044.mp4?px-bps=1385065&px-bufahead=10"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌2 - 02화", "http://mgapi.wecandeo.com/video?k=BOKNS9AQWrHs8cn1KBubvDJ6zJpqzj4LJhZELjSRYNx3mryVWmXPGblq2xJ1FHR8gw7CQFPNodzBGgpMeFADTNt2RaEaa4Gr&ch=true&type=pseudo"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌2 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/79/20140206185136902ospvrqmjclbha.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌2 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/38/20140211190235277h5glqc74sn4ux.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌2 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/54/20140221095110687w8kqeg7tfe2qr.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌2 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/28/201402221006151251fjksa3d9hwql.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌2 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/y/v/yview/31/20140301114416078tk3ydu0kxmcsl.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌2 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/c/o/codeno001/30/20140307151513550o1gvez2e0dpln.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌2 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/39/20140318140058160b1w6i3jgw1n9f.flv"));
                break;
            case "사채꾼 우시지마 시즌3" :
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/20/20160722123352671pa096cwlqm7zv.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/05/20160730001222455eljnuhmck9zgz.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/75/20160804112558334g3lwbeng8vn81.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/96/201608101522431227sl864q0p09gg.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/87/20160827210542461qnl7jbvm2s4kk.flv"));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/93/20160904113140061941c9y0yokqvh.flv"));
                /*listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 07화", ""));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 08화", ""));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 09화", ""));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 10화", ""));
                listArr.add(new ListViewItem2("사채꾼 우시지마 시즌3 - 11화", ""));*/
                break;
            case "5시부터 9시까지 나를 사랑한 스님" :
                listArr.add(new ListViewItem2("5시부터 9시까지 나를 사랑한 스님 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v765bNnTFD4xDgVUWOyWOiO&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("5시부터 9시까지 나를 사랑한 스님 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v90695RwYwdllJU6RAys66J&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("5시부터 9시까지 나를 사랑한 스님 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=ve170UYzqqT9VTmjaXHxWWW&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("5시부터 9시까지 나를 사랑한 스님 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=vb4daZHZSTUSHHCskrUBTzd&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("5시부터 9시까지 나를 사랑한 스님 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=va8b2VxSVF6VH6HBxNMF2EV&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("5시부터 9시까지 나를 사랑한 스님 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v4fbfzPzSzSmrPDcrSoYcoI&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("5시부터 9시까지 나를 사랑한 스님 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v4e62b7cd6cd7L9EZvwubEd&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("5시부터 9시까지 나를 사랑한 스님 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v60c7FCFhmFFkxklu8uxmul&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("5시부터 9시까지 나를 사랑한 스님 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v715dLrleZLZLnHTrqrfOOl&play_loc=undefined&alert=true"));
                listArr.add(new ListViewItem2("5시부터 9시까지 나를 사랑한 스님 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?vid=v392e5LCa0eeWrLD1a1arD0&play_loc=undefined&alert=true"));
                break;
            /*case "스콜피온" :
                listArr.add(new ListViewItem2("아네고 - 01화", ""));
                listArr.add(new ListViewItem2("아네고 - 02화", ""));
                listArr.add(new ListViewItem2("아네고 - 03화", ""));
                listArr.add(new ListViewItem2("아네고 - 04화", ""));
                listArr.add(new ListViewItem2("아네고 - 05화", ""));
                listArr.add(new ListViewItem2("아네고 - 06화", ""));
                listArr.add(new ListViewItem2("아네고 - 07화", ""));
                listArr.add(new ListViewItem2("아네고 - 08화", ""));
                listArr.add(new ListViewItem2("아네고 - 09화", ""));
                listArr.add(new ListViewItem2("아네고 - 10화", ""));
                listArr.add(new ListViewItem2("아네고 - 11화", ""));
                break;*/
        }


        return listArr;
    }
}
