package com.freeking.replaymid2.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;

import com.freeking.replaymid2.R;
import com.freeking.replaymid2.activity.MidListActivity;
import com.freeking.replaymid2.adapter.GridViewAdapter;
import com.freeking.replaymid2.model.GridViewItem;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Administrator on 2016-07-11.
 */
public class Fragment03 extends Fragment {
    private String TAG = "Fragment03_TAG ";
    private ArrayList<GridViewItem> gridArr;
    private GridView gridView;

    private EditText editText;
    private GridViewAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        int portrait_width_pixel= Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        //boolean phoneNumFlag = false;
        boolean usimFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int contactsCnt = c.getCount();
        if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        //if(operator == null || operator.equals("")){ operatorFlag = true; }
        if (telManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
            Log.d(TAG, "유심 x");
            usimFlag = true;
        } else {
            Log.d(TAG, "유심 o");
            usimFlag = false;
        }
        //Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if (contactsFlag && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment01, container, false);

            gridArr = new ArrayList<GridViewItem>();
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/87/41/21/57_3874121_poster_image_1472705712123.jpg","빅토리아 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/16/64/57_661664_poster_image_1401436415813.jpg","로빈후드 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/26/51/57_662651_poster_image_1401436545130.jpg","로빈후드 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/95/54/57_659554_poster_image_1423901379202.jpg","닥터후 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/04/42/57_660442_poster_image_1423902318828.jpg","닥터후 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/16/08/57_661608_poster_image_1423905198660.jpg","닥터후 시즌3"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/21/44/57_662144_poster_image_1423905907004.jpg","닥터후 시즌4"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/36/44/57_663644_poster_image_1423906863771.jpg","닥터후 시즌5"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/45/07/57_664507_poster_image_1423980505582.jpg","닥터후 시즌6"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/11/14/57_671114_poster_image_1423981685556.jpg","닥터후 시즌7"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/58/57_673558_poster_image_1423982486763.jpg","닥터후 시즌8"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/24/93/57_2312493_poster_image_1438917002746.jpg","닥터후 시즌9"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/73/15/43/57_1731543_poster_image_1400724406150.jpg","하우스 오브 카드 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/73/15/48/57_1731548_poster_image_1439967848170.jpg","하우스 오브 카드 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/86/58/78/57_1865878_poster_image_1425007468819.jpg","하우스 오브 카드 시즌3"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/48/83/49/57_2488349_poster_image_1457689666990.jpg","하우스 오브 카드 시즌4"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/48/54/57_664854_poster_image_1424154401712.jpg","셜록 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=nct68x86&q=http://sstatic.naver.net/keypage/image/dss/57/67/31/58/57_673158_poster_image_1435114809818.jpg","셜록 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=nct68x86&q=http://sstatic.naver.net/keypage/image/dss/57/66/94/76/57_669476_poster_image_1424155154272.jpg","셜록 시즌3"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/30/00/57_673000_poster_image_1434697272851.jpg","마이 매드 팻 다이어리 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/86/45/96/57_1864596_poster_image_1417576716682.jpg","마이 매드 팻 다이어리 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/22/53/32/57_2225332_poster_image_1417576243108.jpg","마이 매드 팻 다이어리 시즌3"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/19/36/57_661936_poster_image_1401689467998.jpg","스킨스 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/53/57_662253_poster_image_1401689565649.jpg","스킨스 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/26/75/57_662675_poster_image_1401689627853.jpg","스킨스 시즌3"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/36/57/57_663657_poster_image_1401689702728.jpg","스킨스 시즌4"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155737.jpg","스킨스 시즌5"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/171/171676.jpg","스킨스 시즌6"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/180/180693.jpg","스킨스 시즌7"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/27/12/57_662712_poster_image_1401437163269.jpg","마법사 멀린 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155074.jpg","마법사 멀린 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/88/31/57_668831_poster_image_1401771817864.jpg","마법사 멀린 시즌3"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/88/32/57_668832_poster_image_1401771904933.jpg","마법사 멀린 시즌4"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/176/176470.jpg","마법사 멀린 시즌5"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/46/49/57_674649_poster_image_1403232833883.jpg","피키 블라인더스 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/18/45/62/57_2184562_poster_image_1415770061757.jpg","피키 블라인더스 시즌2"));
            gridArr.add(new GridViewItem("http://ia.media-imdb.com/images/M/MV5BOTQ3OGJjNGItZGNmYi00MGU0LTg2MGUtNzNlOTgxZmU4MTE1XkEyXkFqcGdeQXVyMjExMjk0ODk@._V1_UX182_CR0,0,182,268_AL_.jpg","아이스크림 걸스"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155098.jpg","빙 휴먼 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/45/23/57_664523_poster_image_1401439863190.jpg","빙 휴먼 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/65/62/57_666562_poster_image_1401687702350.jpg","빙 휴먼 시즌3"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/97/95/57_669795_poster_image_1401429624374.jpg","빙 휴먼 시즌4"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/180/180711.jpg","빙 휴먼 시즌5"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/34/96/57_663496_poster_image_1424086606309.jpg","미스핏츠 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/57/10/57_665710_poster_image_1424087199041.jpg","미스핏츠 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/90/83/57_669083_poster_image_1424155191389.jpg","미스핏츠 시즌3"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/23/74/57_672374_poster_image_1424155569129.jpg","미스핏츠 시즌4"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/46/70/57_674670_poster_image_1424155985156.jpg","미스핏츠 시즌5"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/48/58/57_664858_poster_image_1401440603404.jpg","IT 크라우드 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/48/57/57_664857_poster_image_1401440620147.jpg","IT 크라우드 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155719.jpg","IT 크라우드 시즌3"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155719.jpg","IT 크라우드 시즌4"));
            gridArr.add(new GridViewItem("http://dbscthumb.phinf.naver.net/3989_000_1/20150715204837575_M40WK1J0R.jpg/57_664858_poster.jpg?type=m250&wm=N","IT 크라우드 the last byte"));


            //gridArr.add(new GridViewItem("","마이 매드 팻 다이어리 시즌1"));

            editText = (EditText)view.findViewById(R.id.anilist_edit);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String text = editText.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.filter(text);
                }
            });


            gridView = (GridView)view.findViewById(R.id.gridview);
            adapter = new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem);
            gridView.setAdapter(adapter);

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(), MidListActivity.class);
                    intent.putExtra("title", gridArr.get(position).getTitle());
                    intent.putExtra("type", "youngd");
                    startActivity(intent);
                }
            });
        }

        return view;

    }

}
