package replaymid3.jos.com.replaymid3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import replaymid3.jos.com.replaymid3.R;
import replaymid3.jos.com.replaymid3.model.ListViewItem;

/**
 * Created by Administrator on 2016-07-06.
 */
public class ListViewAdapter extends BaseAdapter {

    Context context;
    private LayoutInflater inflater;
    private ArrayList<ListViewItem> listArr;

    public ListViewAdapter(Context context, LayoutInflater inflater, ArrayList<ListViewItem> listArr) {
        this.context = context;
        this.inflater = inflater;
        this.listArr = listArr;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.listviewitem, null);
        if(convertView != null){


            //ImageView imageView = (ImageView)convertView.findViewById(R.id.listview_img);
            TextView textView = (TextView)convertView.findViewById(R.id.listview_tv);
            ImageView imageView = (ImageView)convertView.findViewById(R.id.listview_img);

            String data = listArr.get(position).getTitle();
            String imgUrl = listArr.get(position).getImgUrl();

            Picasso.with(context).load(imgUrl).into(imageView);
            textView.setText(data);

        }

        return convertView;
    }



    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
