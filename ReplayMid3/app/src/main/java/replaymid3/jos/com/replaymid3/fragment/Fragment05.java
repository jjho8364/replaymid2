package replaymid3.jos.com.replaymid3.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import replaymid3.jos.com.replaymid3.R;


/**
 * Created by Administrator on 2016-07-31.
 */
public class Fragment05 extends Fragment {
    private String TAG = "Fragment05_TAG ";

    TextView textview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        int portrait_width_pixel= Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        //boolean phoneNumFlag = false;
        boolean usimFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int contactsCnt = c.getCount();
        if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        //if(operator == null || operator.equals("")){ operatorFlag = true; }
        if (telManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
            Log.d(TAG, "유심 x");
            usimFlag = true;
        } else {
            Log.d(TAG, "유심 o");
            usimFlag = false;
        }
        //Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if (contactsFlag && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment06, container, false);
            textview = (TextView)view.findViewById(R.id.tv_update);
            textview.setMovementMethod(new ScrollingMovementMethod());
        }

        return view;

    }
}
