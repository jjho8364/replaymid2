package replaymid3.jos.com.replaymid3.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;


import java.util.ArrayList;
import java.util.Locale;

import replaymid3.jos.com.replaymid3.R;
import replaymid3.jos.com.replaymid3.activity.MidListActivity;
import replaymid3.jos.com.replaymid3.adapter.GridViewAdapter;
import replaymid3.jos.com.replaymid3.model.GridViewItem;

/**
 * Created by Administrator on 2016-07-31.
 */
public class Fragment04 extends Fragment {
    private String TAG = "Fragment04_TAG ";
    private ArrayList<GridViewItem> gridArr;
    private GridView gridView;

    private EditText editText;
    private GridViewAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        int portrait_width_pixel= Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        //boolean phoneNumFlag = false;
        boolean usimFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int contactsCnt = c.getCount();
        if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        //if(operator == null || operator.equals("")){ operatorFlag = true; }
        if (telManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
            Log.d(TAG, "유심 x");
            usimFlag = true;
        } else {
            Log.d(TAG, "유심 o");
            usimFlag = false;
        }
        //Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if (contactsFlag && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment01, container, false);

            gridArr = new ArrayList<GridViewItem>();
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/29/63/21/57_2296321_poster_image_1454468292910.jpg", "무미랑전기(판빙빙)"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/34/22/54/57_2342254_poster_image_1441703751368.jpg","랑야방:권력의기록"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/contents?size=f91x121&quality=10&q=http%3A%2F%2Fsstatic.naver.net%2Fkeypage%2Fimage%2Fdss%2F57%2F66%2F82%2F15%2F57_668215_poster_image_1396867528958.jpg","보보경심1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/91/83/93/57_1918393_poster_image_1418273324965.jpg","보보경심2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/95/10/57_669510_poster_image_1401691862186.jpg","옹정황제의 여인(후궁견환전)"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154000.jpg","장난스런 키스 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154002.jpg","장난스런 키스 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/11/56/57_671156_poster_image_1402634820076.jpg","애정공우 시즌1"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/82/17/57_668217_poster_image_1402634946901.jpg","애정공우 시즌2"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/75/74/57_2317574_poster_image_1421820991338.jpg","소년신탐적인걸"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/67/16/57_666716_poster_image_1429607570436.jpg","연애의 조건"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/44/20/57_664420_poster_image_1404540348156.jpg","포말지하"));
            //gridArr.add(new GridViewItem("","장난스런키스"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/29/83/49/57_2298349_poster_image_1433237763778.jpg","무신 조자룡 2016(윤아)"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/33/18/23/57_2331823_poster_image_1454398731326.jpg","신조협려 2014"));
            //gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/01/19/57_670119_poster_image_1436780581041.jpg","소오강호 2013"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/168/168286.jpg","의천도룡기"));
            //gridArr.add(new GridViewItem("","장난스런키스"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/01/74/57_660174_poster_image_1401692048984.jpg","황제의 딸 1부"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/16/71/57_661671_poster_image_1401692090791.jpg","황제의 딸 2부"));
            gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/178/178451.jpg","황제의 딸 3부"));
            // 테스트 코드
            //gridArr.add(new GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/178/178451.jpg","테스트"));

            editText = (EditText)view.findViewById(R.id.anilist_edit);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String text = editText.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.filter(text);
                }
            });

            gridView = (GridView)view.findViewById(R.id.gridview);
            adapter = new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem);
            gridView.setAdapter(adapter);

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(), MidListActivity.class);
                    intent.putExtra("title", gridArr.get(position).getTitle());
                    intent.putExtra("type", "joongd");
                    startActivity(intent);
                }
            });
        }

        return view;

    }
}
